package com.tonyx.web;

/**
 * Created by tonyxian on 4/5/2019.
 */
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class TestEndpointController {

    @RequestMapping("/")
    public String index() {
        return "Test Endpoint";
    }

}
